import unittest, xmlrunner
from client import *


class NetworkUnitTest(unittest.TestCase):
    def test_read_user_info(self):
        response = read_user_info("user_dat.json")
        self.assertIsInstance(response, dict)
        self.assertEqual(response["u_name"], "crashoverride")
        self.assertEqual(response["total_posts"], 123)

    def test_read_non_json(self):
        response = read_user_info("user_text.txt")
        self.assertEqual(response, "Invalid format")

    def test_read_no_file(self):
        response = read_user_info("missing_file.dat")
        self.assertEqual(response, "File does not exist")

    def test_sending_data(self):
        u_dat = {
            "usr_cred" : "hunter2",
            "u_name" : "crashoverride",
            "total_posts": 123
        }
        response = send_user_data(u_dat)
        self.assertTrue(response, "Welcome crashoverride you have 123 posts.")
    
    def test_sending_bad_cred(self):
        u_dat = {
            "usr_cred" : "12345",
            "u_name" : "crashoverride",
            "total_posts": 123
        }
        response = send_user_data(u_dat)
        self.assertTrue(response, "Authentication Error")

    def test_sending_bad_dat(self):
        u_dat = {
            "usr_cred" : "hunter2",
            "u_name" : "crashoverride"
        }
        response = send_user_data(u_dat)
        self.assertTrue(response, "Invalid credentials")

    def test_logon(self):
        response = logon("user_dat.json")
        self.assertTrue(response, "Welcome crashoverride you have 123 posts.")

    def test_bad_logon(self):
        response = logon("user_text.txt")
        self.assertTrue(response, "Invalid format")

if __name__ == '__main__':
    unittest.main()

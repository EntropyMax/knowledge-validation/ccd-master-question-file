import json
import pickle
import socket
'''
The objective of this task is to read a user's data file and use it to log in to a server running on port 5000.

Create the following three functions:

read_user_info - Given a file name, open a json file and return its contents
                as a json formatted dictionary using the json module.
                If the file does not exist return "File does not exist".
                If the file is not properly json formatted return "Invalid format"
send_user_data - Given a user's data, connect to the server, and send the user's credentials (usr_cred) to the server.
                If the server responds with "Authentication successful" send the rest of the json data, excluding
                the user's credentials after being pickled by the pickle module. return the server's response as a string.

logon          - given a file name, use the above functions to return the server's response from a logon attempt.

'''

host = "network-server"
port = 5000


def read_user_info(file_name):
    pass

def send_user_data(data):
    pass

def logon(file_name):
    pass
import socket
from student_helper import *

# Refer to README.md for the problem statement

portpair = ('network-server', 5000) # webserver is running on port 5000 of the grader

def send_the_data():
    data_to_send = serialize_dict_to_data(SCORES)

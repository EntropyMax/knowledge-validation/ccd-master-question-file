# KSAT List
This question is intended to evaluate the following topics:
- A0117 - Develop client/server network programs.
- A0019 - Integrate functionality between multiple software components.
- A0018 - Analyze a problem to formulate a software solution.
- A0626 - String-based Protocol
- S0096 - Utilize the Socket library.

# Task
Set up TCP communication using the python module socket.

The server has a simple string:
s = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'

You need to send it data_to_send in order to help it decrypt the string.

Split the server's response into the string component and the integer component and return them as str and int
example - a server response of 
  b'(b"TEST WORDS PLS IGNORE", 64)'
should return a string "TEST WORDS PLS IGNORE" and 64

# Network Communication Map
```text
CLIENT ----> SERVER

CLIENT sends the serialized string

CLIENT <---- SERVER

SERVER returns the response string

Connection ends
```

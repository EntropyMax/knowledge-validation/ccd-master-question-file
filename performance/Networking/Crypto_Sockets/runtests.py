import unittest
from client import *

valid_msg = [
        b"A really secret message. Not for prying eyes.",
        b"Here is the information you need.",
        b"The eagle is in the nest.",
        b"I need to return some video tapes.",
        b"The biscuit has changed hands.",
        b"The spider has many eyes.",
        b"The red fox trots quietly at midnight.",
        b"The asset is invalidated.",
        b"Please direct me to the nearest safe house.",
        "Potential Intrusion Event Detected."
    ]


class NetworkUnitTest(unittest.TestCase):
    def test_create_fernet_and_find_sign_key_wrong_ip(self):
        key = b"I don't know you, check your IP address."
        with self.assertRaises(ValueError) as cm:
            create_fernet_and_find_sign_key(key)
        self.assertEqual(key, cm.exception.args[0])

    def test_create_fernet_and_find_sign_key_intrusion_event(self):
        error_msg = "Potential Intrusion Event Detected."
        key = b"123DE45^-h"
        with self.assertRaises(ValueError) as cm:
            create_fernet_and_find_sign_key(key)
        self.assertEqual(error_msg, cm.exception.args[0])

    def test_crypto(self):
        response = dowork()
        self.assertTrue(response in valid_msg)


if __name__ == '__main__':
    unittest.main()

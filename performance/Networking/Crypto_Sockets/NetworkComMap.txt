Use this example for understanding the sequence that the Client/Server will communicate.

CLIENT: Send your IP address. ex b"127.0.0.1"

SERVER: Will send back a key to create your Fernet object.
ex: b'E80Map25lE-ExamPLE_eXamPle-x_EXAMPLE='

SERVER: Will send back one of two messages. 
		1. On success, you will receive a key. 
        ex: b'E80Map25lE-ExamPLE_eXamPle-x_EXAMPLE='
		2. Unsuccessful, you will receive b"I dont know you, check your IP address."

CLIENT: Send the signing key from the Fernet object. 
ex: b"K\xd5\xc7\x0f\xa0\xe2\\'\x8f-\xdbF\xff\x8aj\x81"

SERVER: Will send back one of two messages. 
		1. If you have the correct signing key, you will receive a encrypted message.
		ex: b'gAAAAABeglwZ_McoAA-eFQyWi_zjzuuycblmQiXrpCfGPSUgU8HbfWIH0ael-rtGJQHlfDRTEL51H7KM-20VS7ivtQtFXl0lnpAaGlnlR0ls6DjGtiu_A6gDpbUli8kATCCbWWxN-PFd'
		2. If you have the incorrect signing key, you will receive b"Wrong signing key".

CLIENT: You will decrypt the previous message using your Fernet object and send back the decrypted message to confirm.
ex: b"I need to return some video tapes."

SERVER: Will send back one of two messages. 
		1. On success, you will receive "Success! Message confirmed:" followed by some xml to show you passed.
		2. Unsuccessful, you will receive "INVALID Message: " followed by some xml to show an unseccussful attempt.



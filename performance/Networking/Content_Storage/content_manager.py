import socket

"""
You have been tasked with creating a server that triggers clients to send data to it, saving that data as it is received.

Create the function contact clients that takes no parameters. This function will contact a server located at host and port and send it srv_port, the port that your server
    is listening on.

Create the function serve_clients that takes no parameters. This function will call the function contact_clients to signal
    clients to communicate with you. Listen on that port for incoming connections and recieve incoming data. Save incoming data as a file
    with the name sent by the client with the content sent. Refer to the NetworkComMap file for a detailed description of the communication.

    The client will message with the following format: file name\nfile contents
    example: example.txt\na,b,c\n1,2,3\n
    would be saved as a file named example.txt with the following contents:

    a,b,c
    1,2,3

    After handling 2 requests the server will clean up and return.
"""

host = ""
port = 5000
srv_port = 1337

def contact_clients():
    pass

def serve_clients():
    pass

import unittest, xmlrunner
from content_manager import *

file_1_name = "riddle.txt"
file_1 = "How much wood could a wood chuck chuck if a wood chuck could chuck wood?\n"
file_2_name = "setup.dat"
file_2 = "startingPosX=330\nstartingPosY=400\ngraphicsAPI=dx12\n"

class NetworkUnitTest(unittest.TestCase):
    def test_server(self):
        serve_clients()
        try:
            with open(file_1_name) as f1:
                f1_cont = f1.read()
            self.assertEqual(f1_cont, file_1)
        except FileNotFoundError:
            self.fail("riddle.txt was not found")

        try:
            with open(file_2_name) as f2:
                f2_cont = f2.read()
            self.assertEqual(f2_cont, file_2)
        except FileNotFoundError:
            self.fail("setup.dat was not found")

if __name__ == '__main__':
    unittest.main()

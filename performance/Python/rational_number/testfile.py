"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0382 - Create a class constructor or destructor
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0048 - Implement a function that receives input parameters.
  S0081 - Implement a looping construct.
  S0110 - Implement error handling.
  S0082 - Implement conditional control flow constructs."""
"""
Create a class called RationalNumber for performing arithmetic with fractions.

Use integer variables to represent the data of the class: the numerator and the denominator.
Provide a constructor that enables an object of this class to be initialized when it is declared. The
constructor should contain default values (i.e., 0/1) in case no initializers are provided, and should store the
fraction in reduced form (i.e., the fraction 2/4 would be stored in the object as 1/2)

Provide the following methods in RationalNumber:
    add -              adds two RationalNumbers. The result should be stored in reduced form.
    subtract -         subtracts two RationalNumbers. The result should be stored in reduced form.
    multiply -        multiplies two RationalNumbers. The result should be stored in reduced form.
    divide -           divides two RationalNumbers. The result should be stored in reduced form.
    printFraction -    returns RationalNumbers as a string in the form a/b, where a is the numerator and b is the denominator, ex. "1/2" or "11/25".
    printDecimal -     returns RationalNumbers in floating-point format rounded to 2 decimal numbers, ex 1.00 or 2.51.

Do not use a module instead of implementing the above features.
"""


class RationalNumber:
    pass


"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0026 - Utilize standard library modules.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0082 - Implement conditional control flow constructs."""
'''
The computeVolume function will compute the volume of two different 3-dimensional items
and return the total volume of those items. The 3-dimensional items are a cuboid and a sphere.

The function will receive two parameters and these parameters could be passed in any order so
this needs to be accounted for.

1. an integer > 0 - represents a radius of a sphere
2. a three item list of whole number each > 0 containing a length, width, and height - represents a cuboid.

The formula for a sphere is 4/3 * PI * radius^3
The formula for a cuboid is length * width * height

Ensure you utilize the proper python modules for PI.

After computing the volume of the sphere, round the result to a whole number.

If any of the parameters are invalid, the function should return the string "INVALID".
	- make sure that one parameter is an integer and the other a list
	- make sure the list is exactly 3 in length
	- make sure all whole number are > 0
	
Assume all values in the list are whole numbers - you do not need to verify they are whole numbers

'''

def computeVolume(item1, item2):
	return 0	
	
	

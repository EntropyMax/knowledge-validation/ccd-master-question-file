"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
'''
In the poker game of guts, the highest card is an Ace and lowest is a 2. In guts each player is given
three cards and you go head to head with your opponents.  The highest hand wins.  The ranking of hands is in 
this order:
1. Highest three of a kind (if any)  		Winner: 6,6,6        Not winner: 5,5,5
2. Highest two of a kind (if any)	 		Winner: 7,7,5		 Not winner: 5,5,6
3. High card (person with highest card)		Winner: 11,7,5		 Not winner: 10,4,2

This game of guts will only have two players to keep it simple.  Assume no two players will ever have
the same pair (if there is a pair).  And no two players will ever have the same high card.

The findGutsWinner function will receive two lists that represent two guts hands.  Each list will contain three numbers
from 2 to 14.  Where 14 represents an Ace, 13 a King etc.

For example hand1 may contain [14,2,7] and hand2 may contain [3,3,14] in which hand2 would be highest because it had two of a kind.

Determine which of the two hands is the highest, and return the list containing that hand.

Assume there are no invalid numbers in each list.

If any list is more or less than three elements, return an empty list.

'''


def findGutsWinner(hand1, hand2):
	return []     

import unittest
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_Node(self):
        testNode = Node("test")
        self.assertTrue(hasattr(testNode, "name"), "Expected name attribute not found")
        self.assertTrue(hasattr(testNode, "next"), "Expected next attribute not found")
        self.assertEqual(type(testNode.name), type(""))
        
    def test_linked_list(self):
        res = processNames(('Joe','Jack','Aaron','Bill','Sally','George'))
        ordList = ['Aaron','Bill','George','Jack','Joe','Sally']
        for name in ordList:
            try:
                self.assertEqual(name, res.name)
                res = res.next
            except:
                #Node class defined incorrectly or not imported in testFile.py
                #or, linked-list improperly implemented
                self.assertEqual(1, 0)
                return

        # res should be None
        self.assertEqual(res, None)

        res = processNames(('Joe','Jack','Aaron','Joe','Bill','Sally','George'))
        ordList = ['Aaron','Bill','George','Jack','Sally']
        for name in ordList:
            self.assertEqual(name, res.name)
            res = res.next

        # res should be None
        self.assertEqual(res, None)

    def test_duplicate_nodes(self):
        res = processNames(('Joe','Joe','Aaron','Jack','Bill','Sally','George','Aaron'))
        ordList = ['Bill','George','Jack','Sally']
        for name in ordList:
            self.assertEqual(name, res.name)
            res = res.next

        # res should be None
        self.assertEqual(res, None)

        res = processNames(('Joe','Able','Aaron','Jack','Bill','Sally','George','Sally'))
        ordList = ['Aaron','Able','Bill','George','Jack','Joe']
        for name in ordList:
            self.assertEqual(name, res.name)
            res = res.next

        # res should be None
        self.assertEqual(res, None)


if __name__ == '__main__':
    unittest.main()

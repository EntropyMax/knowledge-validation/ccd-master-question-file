"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  A0564 - Multi-threading
  S0382 - Create a class constructor or destructor
  S0026 - Utilize standard library modules.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++)."""
"""
** NOTE: THE SOLUTION FOR THIS PROBLEM IS BASED ON A SPECIFICALLY DEFINED ALGORITHM EXPLAINED BELOW. ALTHOUGH THE AUTOMATED TESTS
** MAY BE SUCCESSFUL, YOUR SOLUTION WILL RECEIVE A MANUAL REVIEW FOR FINAL DETERMINATION OF PASS/FAIL

Write a multi-threaded program that demonstrates ***RLock***.

Create two thread classes: AddThread and MultiplyThread.

Class AddThread should increment a global variable by 1,
sleep for a random time (one to 3 seconds) and increment the variable again.

Class MultiplyThread should double the
global variable, sleep for a random time (one to 3 seconds) and double the variable again.

Protect these critical sections with a lock.

After both threads terminate, print the final value of the shared variable.
"""

import threading, time, random

class AddThread( threading.Thread ):
    pass

class MultiplyThread( threading.Thread ):
    pass

if __name__ == "__main__":
    number = 1

    lock = threading.RLock()

    thread1 = AddThread("thread1", lock)
    thread2 = MultiplyThread("thread2", lock)

    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()

    print("Number is: {}".format(number))

#!/usr/bin/env python

import unittest
from testfile import *

class PerformanceTester(unittest.TestCase):
    def test_simple_graph(self):
        two_node_graph_node_check = [(0,5), (1,4)]
        two_node_graph_edge_check = [(0,1,2),(1,0,3)]
        graph = WeightedGraph()
        graph.add_node(0, 5)
        graph.add_node(1, 4)
        graph.add_edge(0, 1, 2)
        graph.add_edge(1, 0, 3)
        self.assertTrue(all(item in two_node_graph_node_check for item in graph.get_nodes()))
        self.assertTrue(all(item in two_node_graph_edge_check for item in graph.get_edges()))

        # delete a node
        graph.delete_node(1)
        self.assertEqual(graph.get_nodes(), [(0,5)])
        self.assertEqual(graph.get_edges(), [])

        # delete the last node
        graph.delete_node(0)
        self.assertEqual(graph.get_nodes(), [])
        self.assertEqual(graph.get_edges(), [])

        # ensure no error when removing a node that doesnt exist
        graph.delete_node(0)
        self.assertEqual(graph.get_nodes(), [])
        self.assertEqual(graph.get_edges(), [])
    
    def test_traverse(self):
        graph = WeightedGraph()
        graph.add_node(0, 5)
        graph.add_node(1, 4)
        graph.add_edge(0, 1, 2)
        graph.add_edge(1, 0, 3)

        graph.add_node(2, 4)
        graph.add_edge(1, 2, 1)
        graph.add_edge(2, 1, 2)

        graph.add_node(3,9)
        graph.add_edge(0, 3, 1)
        graph.add_edge(3, 3, 2)
        graph.add_edge(2, 3, 2)

        graph.add_node(4, 7)
        graph.add_edge(2, 4, 5)

        graph.add_node(5, 10)
        graph.add_edge(1, 5, 17)
        graph.add_edge(5, 4, 33)


        self.assertTrue(all( item in [(0, 1, 2), (0, 3, 1), (1, 0, 3), (1, 2, 1), (1, 5, 17), (2, 1, 2), (2, 3, 2), (2, 4, 5), (3, 3, 2), (5, 4, 33)] for item in graph.get_edges()))

        path = graph.traverse(0, 4)

        self.assertEqual(path, [0, 1, 2, 4])

        self.assertEqual(graph.traverse(4, 0), None)

    def test_traverse_missing_node(self):
        graph = WeightedGraph()
        graph.add_node(0, 5)

        self.assertEqual(graph.traverse(0, 1), None)

if __name__ == '__main__':
	unittest.main()
"""
This question is intended to evaluate the following topics:
  A0061 - Create and implement functions to meet a requirement.
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0026 - Utilize standard library modules.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0081 - Implement a looping construct."""
'''
Write the function count_unique_permutations that receives a string:

The function will return a count of the unique permutations (i.e., order matters) of that string.

Here are some examples:
All permutations of 'aaaa' are 'aaaa', so the function would return 1.

Permutations of 'aaab' are ''aaab', 'aaba', 'abaa', and 'baaa', so the function will 
return 4.

You may not use any mathematical formula to calculate the result
You may not use the in-built itertools.permutations function to generate your permutations


'''

def count_unique_permutations(input_str):
    pass



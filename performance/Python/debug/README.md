# KSAT List
  - A0018 - Analyze a problem to formulate a software solution.
  - A0019 - Integrate functionality between multiple software components.
  - S0017 - Debug a python script.
  - S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  - S0032 - Utilize relational operators to formulate boolean expressions.
  - S0033 - Utilize assignment operators to update a variable.

# Task
A school converts each test score grade to a GPA using the scale below.  When the term is over
they compute the average of all GPAs to determine an overall GPA and grade for the course.

Grade    Score    GPA
A        93-100   4
A-       90-92    3.7
B+       87-89    3.3
B        83-86    3
B-       80-82    2.7
C+       77-79    2.3
C        73-76    2
C-       70-72    1.7
D+       67-69    1.3
D        65-66    1
F        < 65     0

The computeGPA function receives a list of test scores and should:
    1. Compute a GPA for each test
    2. Throw out the two lowest GPAs
    3  Find the overall average GPA for all the GPAs.
    4. Then, based on the overall GPA average, compute a final grade.
    5. Return the average GPA rounded to two decimal positions along with the final grade.
    6. If any score in the list is less than 0 or greater than 100, the function returns "INVALID SCORE"

However, there are several syntax and logical errors in the code below that need to be identified and
corrected before this function executes properly.
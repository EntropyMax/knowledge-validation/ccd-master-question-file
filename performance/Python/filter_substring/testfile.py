"""
This question is intended to evaluate the following topics:
  A0061 - Create and implement functions to meet a requirement.
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
"""
Write the function filter_substring that receives two parameters:
1. list_of_strings - A list of strings to search
2. target - A target string to search for in list of strings

The function will remove all elements where the target string is a sub-string in
any string in the list of strings and return a list with the remaining strings.

For example:

if function is passed: ['aaaa', 'aafaceee', 'face', 'faceeee', 'bbbb'], 'face'
the function will return ['aaaa', 'bbbb']

if the function is passed: ['abcde', 'a'], ''
the function will return []
"""


def filter_substring(list_of_strings, target):
    pass

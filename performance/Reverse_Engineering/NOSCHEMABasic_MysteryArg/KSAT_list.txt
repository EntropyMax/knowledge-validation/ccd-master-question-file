A0343 - Conduct static analysis of a binary to determine its functionality.
A0610 - Disassemble Binary
A0611 - Static Analysis
A0609 - WinDbg Dynamic Analysis
S0179 - Reverse engineer a binary using IDA Pro.
S0180 - Reverse engineer a binary using WinDBG.

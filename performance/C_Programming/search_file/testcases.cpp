#include <gmock/gmock.h>
#include "TestCode.h"

TEST(TestCase2, file_search_test_normalCases)
{

    int res = searchFile("test.txt","test");
    ASSERT_EQ(res, 5);

	res = searchFile("test2.txt","test"); // file file <= 150
    ASSERT_EQ(res, -1);

    res = searchFile("test3.txt","test"); //file doesn't exist
    ASSERT_EQ(res, -2);

    res = searchFile("test4.txt","house"); //no instances of 'house'
    ASSERT_EQ(res, 0);

	res = searchFile("test5.txt", "house");  // file <= 150
	ASSERT_EQ(res, -1);

	res = searchFile("test6.txt", "house");
	ASSERT_EQ(res, 1);
	
}
/*
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0036 - Declare and implement a char * array (string).
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0079 - Validate expected input.
  S0081 - Implement a looping construct.
  S0108 - Utilize post and pre increment/decrement operators.
  S0082 - Implement conditional control flow constructs.
  S0160 - Utilize the standard library.*/
#include <stdio.h>
#include "TestCode.h"

/*
stringToDouble parses a string pointed to by the argument str, interpreting its content as a floating point number and returns its value as a double.
If the input parameter is NULL or empty, the function should return ERROR_INVALID_PARAMETER

**** NOTE: stringToDouble SHOULD NOT CALL THE BUILT-IN C FUNCTION strtod!!!! ****
*/



double stringToDouble(const char *str)
{		

	return 0;   
}
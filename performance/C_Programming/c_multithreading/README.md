# KSAT List
This question is intended to evaluate the following topics:
- A0179 - Implement file management operations.
- K0178 - Understand how to exit a thread in multi-threading programs.
- K0182 - Understand how to handle race conditions in multi-threading programs.
- K0174 - Understand how to implement a pthread in multi-threading programs.
- K0176 - Understand how to implement a join in multi-threading programs.
- K0180 - Understand how to implement a mutex in multi-threading programs.
- K0173 - Understand how to implement a thread in multi-threading programs.
- K0746 - Understand how to open and close an existing file.
- K0747 - Understand how to read, parse write file data.
- S0042 - Open and close an existing file.
- S0043 - Read, parse, write (append, insert, modify) file data.
- S0146 - Utilize mutexes in a multithreaded program.
- S0144 - Utilize threads in a multi-threaded program.

# Task
A prime number is any positive integer that has two factors, 1, and the number itself. e.g.: 2, 3, 5, 7, 11,...

Create a function, FindPrimes, that takes the name of a file and a pointer to an array that you will output prime numbers to, and the size of that array
    Using multithreading, create 10 threads that will share the work of calculating which numbers in the file specified are prime.
    Properly protect resources shared by threads
    After all prime numbers are found, return the prime numbers via the primes parameter passed to FindPrimes

You may modify the header file if needed.

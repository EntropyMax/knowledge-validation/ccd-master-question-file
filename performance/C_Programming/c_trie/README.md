# KSAT List
This question is intended to evaluate the following topics:
 - S0059 - Create and destroy a Tree.
 - S0072 - Find an item in a Tree.
 - S0073 - Add and remove nodes from a Tree.
 - A0018 - Analyze a problem to formulate a software solution.
 - S0024 - Declare and/or implement container data type.
 - S0033 - Utilize assignment operators to update a variable.
 - S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
 - S0052 - Implement a function that returns a single value.
 - S0048 - Implement a function that receives input parameters.
 - S0081 - Implement a looping construct.
 - S0082 - Implement conditional control flow constructs.
 - S0034 - Declare and implement appropriate data types for program requirements.

# Task
Implement a tree that contains words, with the links between nodes being the individual characters in each word. 
note: this question only tests the add a node portion of the KSAT S0073

Implement 4 functions:

- buildTree takes in an array of words and the length of the array. It creates a tree with every word in the array in the tree; the links between nodes are the individual characters in each word (see diagram below). Each node needs to indicate if it can be the end of a word in the tree. The buildTree function will return a pointer to the root of the tree.

- addNode takes in a pointer to a tree and a word; the word is added to the tree.

- findWord takes in a word; it returns 1 if the word exists in the tree and 0 if it does not exist in the tree.

- deleteTree takes in a pointer to the root of a tree; it frees each node within the tree and the root node. The root pointer should be NULL after the function returns.

Diagram:
```
      root
     /    \
    f      c
   /      / \
  o      o   a
 / \    /   / \
x.  o  l   r.  t.
     \  \   \
      t. d.  e.
```
note: letters in the above diagram with a period (.) following it indicate that the letter is the end of a word

Uppercase and lowercase characters should be treated equally when both adding words to the tree and when searching for them in the tree.

Assume there are no numbers or whitespace or punctuation in the words

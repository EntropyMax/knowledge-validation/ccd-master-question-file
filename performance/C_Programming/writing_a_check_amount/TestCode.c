/*
This question is intended to evaluate the following topics:
    A0019 - Integrate functionality between multiple software components.
    A0018 - Analyze a problem to formulate a software solution.
    S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
    S0034 - Declare and implement appropriate data types for program requirements.
    S0032 - Utilize relational operators to formulate boolean expressions.
    S0036 - Declare and implement a char * array (string).
    S0033 - Utilize assignment operators to update a variable.
    S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
    S0052 - Implement a function that returns a single value.
    S0053 - Implement a function that returns a memory reference.
    S0048 - Implement a function that receives input parameters.
    S0090 - Allocate memory on the heap (malloc).
    S0097 - Create and use pointers.
    S0082 - Implement conditional control flow constructs.
    S0160 - Utilize the standard library.
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


/*
Write a function that inputs a numeric check amount (VALUES UP TO $99.99) and writes the word equivalent of the amount.
For example, the amount 52.43 should be written as FIFTY TWO and 43/100.
The amount 0.52 should be written as ZERO and 52/100.
If the check amount is <= 0 it should return NULL.
If the check amount is >= 100 it should return NULL.
*/



char* checkAmount(double amount)
{
    char *checkValueInWords = 0;



    return checkValueInWords;
}
/*
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0035 - Declare and/or implement of arrays and multi-dimensional arrays.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0058 - Create and destroy a Queue.
  S0071 - Enqueue and dequeue a Queue.
  S0053 - Implement a function that returns a memory reference.
  S0048 - Implement a function that receives input parameters.
  S0090 - Allocate memory on the heap (malloc).
  S0097 - Create and use pointers.
  S0091 - Unallocating memory from the heap (free).
  S0081 - Implement a looping construct.
  S0108 - Utilize post and pre increment/decrement operators.
  S0082 - Implement conditional control flow constructs.
  S0156 - Utilize a struct composite data type.
  S0160 - Utilize the standard library.*/
/*
This task will implement a simple queue using a linked list. A numNode struct is defined in the 
TestCode.h file to facilitate the creation of the stack.

Write the function createQueue that receives two parameters:

    1. actions: an int array - this array contains values of 1, 2, or 3 in the even indices 
	   0,2,4 etc that indicate which type of action to do with the queue:
    	  1 - dequeue (remove an item from the queue)
    	  2 - enqueue (add an item to the queue)
    	  3 - empty queue

    	The odd indices will only apply for enqueue actions. If the action received is action
    	enqueue (2), then the next index contains the value to add to the queue. The
    	odd indices have no effect on dequeue (1) or empty queue (3)
    	
    	So if the function receives:  [2, 7, 2, 2, 1, 0, 3, 0], then you would enqueue 7 to
    	the queue, then enqueue 2 to the queue, then remove an item from the queue, then empty the queue.

    2. numActions: the total number of actions supplied in the array. In the above example this would be 4.

The function should iterate the actions array and determine what actions should occur on the queue.  Provide the 
implementation for enqueueing, dequeuing, emptying the queue accordingly.

If any action provided is not 1, 2, or 3, then return NULL.

Assume that the array will always have an even number of elements. 

The function will return a pointer that points to the front of the queue.

*/

#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"


struct numNode * makeQueue(int actions[], int numActions)
{
    
    return NULL;
}


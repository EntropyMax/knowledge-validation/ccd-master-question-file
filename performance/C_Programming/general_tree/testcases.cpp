#include <gmock/gmock.h>
#include <string.h>
#include "TestCode.h"


TEST(TestCase1, GeneralTreeTest_normalCases)
{
	int nums[] = {0,4,4,2,4,5,4,6,0,12,0,14,14,77,14,56};
	struct TreeNode* root = buildTree(nums, sizeof(nums) / sizeof(int));
	struct TreeNode * temp = root;
	int parent = 14;
	int child = 77;

	while(temp != NULL && temp->number != parent)
		temp = temp->nextSibling;

	if(temp == NULL) // parent not found
		ASSERT_EQ(0, 1);
	else  // parent found
	{
		temp = temp->firstChild;
		while(temp != NULL && temp->number != child)
			temp = temp->nextSibling;

		if(temp == NULL)  //child not found
			ASSERT_EQ(2, 3);
		else
			ASSERT_EQ(temp->number, child);
	}

	temp = root;
	parent = 4;
	child = 6;

	while(temp != NULL && temp->number != parent)
		temp = temp->nextSibling;

	if(temp == NULL) // parent not found
		ASSERT_EQ(0, 1);
	else  // parent found
	{
		temp = temp->firstChild;
		while(temp != NULL && temp->number != child)
			temp = temp->nextSibling;

		if(temp == NULL)  //child not found
			ASSERT_EQ(2, 3);
		else
			ASSERT_EQ(temp->number, child);
	}
}

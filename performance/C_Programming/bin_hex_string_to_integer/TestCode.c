/*
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0036 - Declare and implement a char * array (string).
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0081 - Implement a looping construct.
  S0108 - Utilize post and pre increment/decrement operators.
  S0082 - Implement conditional control flow constructs.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "TestCode.h"

/*

Write a function bin_hex_StrToInt32 that receives a string containing either a binary or hex number. 
The function must return the string's integer value. 

Note that a string that contains ONLY '1' and '0' can be considered as a binary or a hex string. 
For example, "10" can be a binary number equivalent to 2 or a hex number equivalent to 16.
If the string is contains ONLY '1' and '0', the default will be ***binary***.

If the input parameter is empty or invalid value, the function should return ERROR_INVALID_PARAMETER

Note: Do not call built-in library functions that accomplish these tasks automatically.
*/



int bin_hex_StrToInt32(const char * s)
{
	return 0;
}


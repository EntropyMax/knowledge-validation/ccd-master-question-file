# KSATs
 - S0109 - Implement a program that uses standard main() command line arguments.
 - A0179 - Implement file management operations.
 - S0042 - Open and close an existing file.
 - S0044 - Create and delete file.
 - T0009 - (U) Analyze, modify, develop, debug and document software and applications in C programming language.
 - A0061 - Create and implement functions to meet a requirement.
 - S0081 - Implement a looping construct.

# Task
Create a program that takes command line arguments and writes them to a file.

The program TestCode will accept a list of words and write them to a file called text.txt.

Implement an option, -f and --filename which uses the next word as the file name that the words are writen to.

Example:

TestCode -f examplefile.txt word1 word2 word3

or

TestCode --filename example.txt word1 word2 word3

All words other than the option and the following filename should be treated as words to write into the output file. If the -f or --filename option is provided but does not contain a file name use the default text.txt file name.

Create a function fileDump that accepts the file name, an array of words, and how many words are in the list. This function will write the words to the specified file. Return 1 on a success and a 0
if either the file name or the word list is null. If the words array is empty the file should still be created.

The words will be written to the file on seperate lines, separted by a newline 

example:

```
word1
word2
word3
```

etc.
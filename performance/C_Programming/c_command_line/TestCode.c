#include <stdio.h>
#include "testcases.h"
#include "TestCode.h"

/* see the README.md for instructions*/


int main(void)
{

    /* DO NOT MOVE OR MODIFY THE FOLLOWING CODE*/
    /* YOUR SOLUTION SHOULD BE CONTAINED ABOVE THIS POINT*/
    if (getenv("CCOMMANDLINETEST") == NULL)
    {
        doTest();
    }

    return 0;
}

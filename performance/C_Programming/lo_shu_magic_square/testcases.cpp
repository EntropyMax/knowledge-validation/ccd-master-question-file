#include <gmock/gmock.h>
#include "TestCode.h"




TEST(TestCase1, isMagicSquareTest)
{
	int square1[ROWS][COLS] = { { 4, 9, 2 },
								{ 3, 5, 7 },
								{ 8, 1, 6 } };

	int square2[ROWS][COLS] = { { 8, 3, 4 },
								{ 1, 5, 9 },
								{ 6, 7, 2 } };

	int square3[ROWS][COLS] = { { 6, 1, 8 },
								{ 7, 5, 3 },
								{ 2, 9, 4 } };

	int square4[ROWS][COLS] = { { 0, 0, 0 },
								{ 0, 0, 0 },
								{ 0, 0, 0 } };

	int square5[ROWS][COLS] = { { 1, 1, 1 },
								{ 1, 1, 1 },
								{ 1, 1, 1 } };

	int square6[ROWS][COLS] = { { 1, 0, 0 },
								{ 0, 1, 0 },
								{ 0, 0, 1 } };

	int square7[ROWS][COLS] = { { 10, 2, 3 },
								{ 4, 15, 6 },
								{ 7, 8, -8 } };

	int square8[ROWS][COLS] = { { 5, 5, 5 },
								{ 5, 5, 5 },
								{ 5, 5, 5 } };



	ASSERT_EQ(true, isMagicSquare(square1));
	ASSERT_EQ(true, isMagicSquare(square2));
	ASSERT_EQ(true, isMagicSquare(square3));
	ASSERT_EQ(false, isMagicSquare(square4));
	ASSERT_EQ(false, isMagicSquare(square5));
	ASSERT_EQ(false, isMagicSquare(square6));
	ASSERT_EQ(false, isMagicSquare(square7));
	ASSERT_EQ(false, isMagicSquare(square8));



}
#pragma once

#ifdef __cplusplus
extern "C" {
#endif
    long long add(int a, int b);
    long long subtract(int a, int b);
    long long multiply(int a, int b);
    long long divide(int a, int b);
    long long modulus(int a, int b);
    int isDigit(const char *input, int sz_input);
    void usage(char *filename);
#ifdef __cplusplus
}
#endif
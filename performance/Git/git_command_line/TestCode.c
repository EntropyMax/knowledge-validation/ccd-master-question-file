#include <stdio.h>
#include <string.h>
#include "TestCode.h"

// These are the basic_calc core functions for a basic_calc program

long long add(int a, int b)
{
    return (long long) a + b;
}

long long subtract(int a, int b)
{
    return (long long) a - b;
}

long long multiply(int a, int b)
{
    return (long long) a * b;
}

long long divide(int a, int b)
{
    return (b != 0) ? (long long) a / b : 0;
}

long long modulus(int a, int b)
{
    return (b != 0) ? (long long) a % b : a;
}

int isDigit(const char *input, int sz_input)
{
    if (input == NULL || *input == '\0')
    {
        return 0;
    }

    for (int i = 0; i < sz_input; i++)
    {
        if (!(input[i] >= '0' && input[i] <= '9'))
        {
            return 0;
        }
    }

    return 1;
}

void usage(char *filename)
{
    printf("USAGE: %s A <symbol> B\n", filename);
    printf("\ta        - The first number for calculation\n");
    printf("\tb        - The second number for calculation\n");
    printf("\t<symbol> - One of +, -, *, /, or %%\n");
}
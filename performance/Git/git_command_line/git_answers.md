# Answers for the git_command-line question

What git command did you use to create and checkout the `finish_basic_calc` branch? 
Please include your current working directory when issuing this command.



What git command did you use to clone the googletest repository?
Please include your current working directory when issuing this command.



What git command did you use to checkout the cloned repository's `master` branch?
Please include your current working directory when issuing this command.



What git command(s) did you use to add changes to a commit? 
Please include your current working directory when issuing this command.



What git command did you use to commit changes to your branch?
Please include your current working directory when issuing this command.



What git command did you use to update your project (exam) repository with your committed changes?
Please include your current working directory when issuing this command.



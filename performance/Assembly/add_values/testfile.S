bits 32

section .text

global _addvalues


; Given three 32 bit integer arrays of 4 elements each, add each element of the first (x) to the corresponding element of
; the second (y) (basically, like adding one vector to another), and store the result in the third (z). - Ex. (in C):
; z[i] = x[i] + y[i];
;
; void __cdecl addvalues(int* x, int* y, int* z)
_addvalues:
	push ebp
	mov ebp, esp

	push edi
	push esi
	push ebx

	mov edi, [ebp+0x8]	; EDI contains x
	mov esi, [ebp+0xc]	; ESI contains y
	mov ebx, [ebp+0x10]	; EBX contains z

	;code begin
	;int3

	;code end

	pop edi
	pop esi
	pop ebx

	mov esp, ebp
	pop ebp

	ret
